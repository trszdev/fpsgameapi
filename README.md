## Run ##


```
#!cmd

run --prefix http://+:8080
```


but this may require administrator rights, so do this instead:


```
#!cmd

run --prefix http://localhost:8080
```