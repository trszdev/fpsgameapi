﻿using FpsGameApi.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FpsGameApi.Controllers;

namespace FpsGameApi.Test
{
    [TestClass]
    public class ReporterControllerTest : ControllerTest
    {
        ControllerFactory Create60Matches(out Server server, out Match[] matches)
        {
            var fac = CreateControllerFactory();
            server = new Server
            {
                Endpoint = "ping.com:80",
                Info = new ServerInfo
                {
                    Name = "my internet",
                    GameModes = new[] { "DEFAULT" }
                }
            };
            fac.CreateServerController().AddServer(server);
            var date = DateTime.Now;
            matches = new Match[60];
            for (var i = 0; i < 60; i++)
            {
                var match = new Match
                {
                    Server = server.Endpoint,
                    Results = new MatchResults
                    {
                        FragLimit = i,
                        GameMode = "DEFAULT",
                        Map = "q3dm6",
                        TimeElapsed = 20,
                        TimeLimit = 40,
                        Scoreboard = new[] {
                        new ScoreboardPlayer { Deaths = 0, Kills = i, Frags = i, Name = "Sister"+i },
                        new ScoreboardPlayer { Deaths = i, Kills = 0, Frags = 0, Name = "Brother"+i },
                    }
                    },
                    Timestamp = date.AddSeconds(i),
                };
                matches[i] = (match);
                fac.CreateMatchController().AddMatch(match);
            }
            return fac;
        }

        [TestMethod]
        public void RecentMatchesNoMatches() 
        {
            var fac = CreateControllerFactory();
            var matches = fac.CreateReportController().RecentMatches().ToList();
            Assert.AreEqual(0, matches.Count);
        }

        [TestMethod]
        public void RecentMatches60Matches()
        {
            Server server; Match[] matches;
            var fac = Create60Matches(out server, out matches);
            var recentMatches = fac.CreateReportController().RecentMatches(60).ToList();
            Assert.AreEqual(50, recentMatches.Count);
            foreach (var match in matches.Take(50))
            {
                var got = fac.CreateMatchController().GetMatch(match.Server, match.Timestamp);
                Assert.AreEqual(match.Results, got);
            }
        }


        [TestMethod]
        public void BestPlayersNoMatches()
        {
            var fac = CreateControllerFactory();
            var players = fac.CreateReportController().BestPlayers().ToList();
            Assert.AreEqual(0, players.Count);
        }

        [TestMethod]
        public void BestPlayers60Matches_LessThan10Matches()
        {
            Server server; Match[] matches;
            var fac = Create60Matches(out server, out matches);
            var bestPlayers = fac.CreateReportController().BestPlayers(60);

            Assert.AreEqual(0, bestPlayers.Count());
        }

        [TestMethod]
        public void BestPlayers60Matches()
        {
            var fac = CreateControllerFactory();
            var server = new Server
            {
                Endpoint = "ping.com:80",
                Info = new ServerInfo
                {
                    Name = "my internet",
                    GameModes = new[] { "DEFAULT" }
                }
            };
            fac.CreateServerController().AddServer(server);
            var date = DateTime.Now;
            for (var i = 0; i < 60; i++)
            {
                var match = new Match
                {
                    Server = server.Endpoint,
                    Results = new MatchResults
                    {
                        FragLimit = i,
                        GameMode = "DEFAULT",
                        Map = "q3dm6",
                        TimeElapsed = 20,
                        TimeLimit = 40,
                        Scoreboard = new[] {
                        new ScoreboardPlayer { Deaths = 1, Kills = 1, Frags = i, Name = "Sister" },
                        new ScoreboardPlayer { Deaths = i, Kills = 0, Frags = 0, Name = "Brother" },
                        new ScoreboardPlayer { Deaths = 0, Kills = 0, Frags = 0, Name = "Daddy" },
                        new ScoreboardPlayer { Deaths = 0, Kills = 666, Frags = 0, Name = "Granny" },
                    }
                    },
                    Timestamp = date.AddSeconds(i),
                };
                fac.CreateMatchController().AddMatch(match);
            }

            var bestPlayers = fac.CreateReportController().BestPlayers(60).ToList();
            Assert.AreEqual(2, bestPlayers.Count);
            Assert.AreEqual(0D, bestPlayers[1].KillToDeathRatio, 0.0001);
            Assert.AreEqual(1D, bestPlayers[0].KillToDeathRatio, 0.0001);
            Assert2.AreEqualSequences(new[] { "Sister", "Brother" }, 
                bestPlayers.Select(x => x.Name));
        }

        [TestMethod]
        public void PopularServersNoMatches()
        {
            var fac = CreateControllerFactory();
            var servers = fac.CreateReportController().PopularServers().ToList();
            Assert.AreEqual(0, servers.Count);
        }
    }
}
