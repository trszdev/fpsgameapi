﻿using FpsGameApi.ViewModels;
using FpsGameApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace FpsGameApi.Test
{
    [TestClass]
    public class MatchControllerTest : ControllerTest
    {
        public void AddGetMatch(ControllerFactory fac, Server server, Match match)
        {
            fac.CreateServerController().AddServer(server);
            fac.CreateMatchController().AddMatch(match);
            var got = fac.CreateMatchController().GetMatch(match.Server, match.Timestamp) as MatchResults;

            Assert.AreEqual(match.Results, got);
        }

        [TestMethod]
        public void AddTwice()
        {
            var fac = CreateControllerFactory();
            var server = new Server
            {
                Endpoint = "rkn.ru:776",
                Info = new ServerInfo
                {
                    Name = "(.*)\"'",
                    GameModes = new[] { "SIGTERM" }
                }
            };
            var match = new Match
            {
                Timestamp = DateTime.Now,
                Server = server.Endpoint,
                Results = new MatchResults
                {
                    FragLimit = 999,
                    GameMode = "SIGTERM",
                    Map = "classic",
                    TimeElapsed = 30,
                    TimeLimit = 60,
                    Scoreboard = new[] {
                        new ScoreboardPlayer { Deaths = 1, Kills = 0, Frags = 0, Name = "--AFK PLAYER--"},
                    }
                },
            };
            AddGetMatch(fac, server, match);
            Assert.IsFalse(fac.CreateMatchController()
                    .AddMatch(match) is OkResult);
            var got = fac.CreateMatchController()
                .GetMatch(match.Server, match.Timestamp) as MatchResults;
            Assert.AreEqual(match.Results, got);
        }


        [TestMethod]
        public void AddGetMatch()
        {
            var server = new Server {
                Endpoint = "rkn.ru:776",
                Info = new ServerInfo {
                    Name = "(.*)\"'",
                    GameModes = new [] { "SIGTERM" }
                }
            };
            var match = new Match {
                Timestamp = DateTime.Now,
                Server = server.Endpoint,
                Results = new MatchResults {
                    FragLimit = 999,
                    GameMode = "SIGTERM", 
                    Map = "classic",
                    TimeElapsed = 30,
                    TimeLimit = 60,
                    Scoreboard = new [] {
                        new ScoreboardPlayer { Deaths = 1, Kills = 0, Frags = 0, Name = "--AFK PLAYER--"},
                    }
                },
            };
            AddGetMatch(CreateControllerFactory(), server, match);
        }

        [TestMethod]
        public void GetUnexistent()
        {
            var fac = CreateControllerFactory();
            var server = new Server
            {
                Endpoint = "74.125.224.72:23",
                Info = new ServerInfo
                {
                    Name = "#goo...",
                    GameModes = new[] { "QA", "USUAL" }
                }
            };
            var match = new Match
            {
                Timestamp = DateTime.Now,
                Server = server.Endpoint,
                Results = new MatchResults
                {
                    FragLimit = 999,
                    GameMode = "USUAL",
                    Map = "classic",
                    TimeElapsed = 30,
                    TimeLimit = 60,
                    Scoreboard = new[] {
                        new ScoreboardPlayer { 
                            Deaths = 1, Kills = 0, Frags = 0, Name = "--AFK PLAYER AGAIN--"
                        },
                    }
                },
            };

            AddGetMatch(fac, server, match);

            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch("pochtarf.co:90", match.Timestamp) is NotFoundResult);
            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch(match.Server, match.Timestamp.AddSeconds(1)) is NotFoundResult);
            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch(match.Server, match.Timestamp.AddSeconds(-1)) is NotFoundResult);
            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch(match.Server, match.Timestamp.AddDays(7)) is NotFoundResult);
            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch(match.Server, match.Timestamp.AddYears(1)) is NotFoundResult);
            Assert.IsTrue(fac.CreateMatchController()
                .GetMatch(server.Info.Name, match.Timestamp) is NotFoundResult);
        }

    }
}
