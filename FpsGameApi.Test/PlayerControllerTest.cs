﻿using FpsGameApi.ViewModels;
using FpsGameApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Test
{
    [TestClass]
    public class PlayerControllerTest : ControllerTest
    {
        [TestMethod]
        public void StatsEmpty()
        {
            var fac = CreateControllerFactory();
            var stats = fac.CreatePlayerController().Stats("Phil");
            Assert.AreEqual(PlayerStats.Empty, stats);
        }

        [TestMethod]
        public void StatsChanged()
        {
            var fac = CreateControllerFactory();
            
            var server = new Server
            {
                Endpoint = "ping.com:80",
                Info = new ServerInfo
                {
                    Name = "my internet",
                    GameModes = new[] { "DEFAULT" }
                }
            };
            var match = new Match
            {
                Server = server.Endpoint,
                Results = new MatchResults
                {
                    FragLimit = 2,
                    GameMode = "DEFAULT",
                    Map = "q3dm6",
                    TimeElapsed = 20,
                    TimeLimit = 40,
                    Scoreboard = new[] {
                        new ScoreboardPlayer { Deaths = 0, Kills = 2, Frags = 2, Name = "Phil"},
                        new ScoreboardPlayer { Deaths = 2, Kills = 0, Frags = 0, Name = "Brother"},
                    }
                },
                Timestamp = DateTime.Now,
            };
            var statsBefore = fac.CreatePlayerController().Stats("Phil");
            fac.CreateServerController().AddServer(server);
            fac.CreateMatchController().AddMatch(match);
            var statsAfter = fac.CreatePlayerController().Stats("Phil");

            Assert.AreNotEqual(statsAfter, statsBefore);
            Assert.AreEqual(new PlayerStats
            {
                AverageMatchesPerDay = 1,
                FavoriteGameMode = match.Results.GameMode,
                AverageScoreboardPercent = 100,
                FavoriteServer = match.Server,
                UniqueServers = 1,
                KillToDeathRatio = -3D,
                LastMatchPlayed = match.Timestamp,
                MaximumMatchesPerDay = 1,
                TotalMatchesPlayed = 1,
                TotalMatchesWon = 1
            }, statsAfter);
        }
    }
}
