﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FpsGameApi.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http.Results;
using FpsGameApi.Controllers;

namespace FpsGameApi.Test
{
    [TestClass]
    public class ServerControllerTest : ControllerTest
    {

        public void AddGetServer(ControllerFactory fac, Server server)
        {
            Assert.IsTrue(fac.CreateServerController()
                .AddServer(server) is OkResult);
            var got1 = fac.CreateServerController()
                .GetServerInfo(server.Endpoint) as ServerInfo;
            var got2 = fac.CreateServerController()
                .GetAllServerInfos().Single();

            Assert.AreEqual(server.Info, got1);
            Assert.AreEqual(server, got2);
        }

        [TestMethod]
        public void AddGetServer()
        {
            AddGetServer(CreateControllerFactory(), new Server
            {
                Endpoint = "почта.рф:27005",
                Info = new ServerInfo
                {
                    Name = "$hak/i//ra",
                    GameModes = new[] { "Lol", "DM" }
                }
            });
        }

        [TestMethod]
        public void GetNonexistent()
        {
            var fac = CreateControllerFactory();
            foreach(var endpoint in new [] {
                "./", "#;1/1--", "<>/", "</", "\0ZEro", "^%d%f%s{}{0}{1}"
            })
                Assert.IsTrue(fac.CreateServerController().GetServerInfo(endpoint) is NotFoundResult);
        }


        [TestMethod]
        public void StatsEmpty()
        {
            var fac = CreateControllerFactory();
            var stats = fac.CreateServerController().Stats("x3dm");
            Assert.AreEqual(ServerStats.Empty, stats);
        }


        [TestMethod]
        public void Stats1Match()
        {
            var fac = CreateControllerFactory();
            var server = new Server{
                Endpoint = "ping.com:80",
                Info = new ServerInfo{
                    Name = "my internet",
                    GameModes = new [] { "DEFAULT" }
                }
            };
            var match = new Match {
                Server = server.Endpoint,
                Results = new MatchResults {
                    FragLimit = 2,
                    GameMode = "DEFAULT", 
                    Map = "q3dm6",
                    TimeElapsed = 20,
                    TimeLimit = 40,
                    Scoreboard = new [] {
                        new ScoreboardPlayer { Deaths = 0, Kills = 2, Frags = 2, Name = "Sister"},
                        new ScoreboardPlayer { Deaths = 2, Kills = 0, Frags = 0, Name = "Brother"},
                    }
                },
                Timestamp = DateTime.Now,
            };
            var statsBefore = fac.CreateServerController().Stats(server.Endpoint);
            fac.CreateServerController().AddServer(server);
            fac.CreateMatchController().AddMatch(match);
            var statsAfter = fac.CreateServerController().Stats(server.Endpoint);

            Assert.AreNotEqual(statsAfter, statsBefore);
            Assert.AreEqual(new ServerStats
            {
                AverageMatchesPerDay = 1,
                MaximumMatchesPerDay = 1,
                AveragePopulation = 2,
                MaximumPopulation = 2,
                TotalMatchesPlayed = 1,
                Top5GameModes = new[] { "DEFAULT" },
                Top5Maps = new[] { "q3dm6" }
            }, statsAfter);
        }

        [TestMethod]
        public void Stats3Matches()
        {
            var timestamp = DateTime.Now;
            var fac = CreateControllerFactory();
            var server = new Server
            {
                Endpoint = "ping.com:80",
                Info = new ServerInfo
                {
                    Name = "my internet",
                    GameModes = new[] { "DEFAULT" }
                }
            };
            var match = new Match
            {
                Server = server.Endpoint,
                Results = new MatchResults
                {
                    FragLimit = 2,
                    GameMode = "DEFAULT",
                    Map = "q3dm6",
                    TimeElapsed = 20,
                    TimeLimit = 40,
                    Scoreboard = new[] {
                        new ScoreboardPlayer { Deaths = 0, Kills = 2, Frags = 2, Name = "Sister"},
                        new ScoreboardPlayer { Deaths = 2, Kills = 0, Frags = 0, Name = "Brother"},
                    }
                },
                Timestamp = timestamp,
            };
           
            var statsBefore = fac.CreateServerController().Stats(server.Endpoint);
            fac.CreateServerController().AddServer(server);
            fac.CreateMatchController().AddMatch(match);
            match.Timestamp = timestamp.AddDays(1);
            fac.CreateMatchController().AddMatch(match);
            match.Results.GameMode = "another gamemode";
            match.Results.Map = "another map";
            match.Timestamp = timestamp.AddDays(2);
            fac.CreateMatchController().AddMatch(match);
            //fac.CreateMatchController().AddMatch(match);
            var statsAfter = fac.CreateServerController().Stats(server.Endpoint);

            Assert.AreNotEqual(statsAfter, statsBefore);
            Assert.AreEqual(new ServerStats
            {
                AverageMatchesPerDay = 1,
                MaximumMatchesPerDay = 1,
                AveragePopulation = 2,
                MaximumPopulation = 2,
                TotalMatchesPlayed = 3,
                Top5GameModes = new[] { "DEFAULT", "another gamemode" },
                Top5Maps = new[] { "q3dm6", "another map" }
            }, statsAfter);
        }


        [TestMethod]
        public void AddTwice()
        {
            var server = new Server
            {
                Endpoint = "cheapservers.co.uk.de.net.com:777",
                Info = new ServerInfo
                {
                    Name = "($)",
                    GameModes = new[] { "HM", "DM", "VM", "CM", "YU", "CHU",
                        "DOR", "SKE", "RP" }
                }
            };
            var fac = CreateControllerFactory();
            AddGetServer(fac, server);
            Assert.IsFalse(fac.CreateServerController()
                    .AddServer(server.Endpoint, new ServerInfo
                    {
                        Name = "newname",
                        GameModes = new string[0],
                    }) is OkResult);
            var got = fac.CreateServerController().GetServerInfo(server.Endpoint) as ServerInfo;
            Assert.AreEqual(server.Info, got);
        }
    }
}
