﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FpsGameApi.Models;
using FpsGameApi.ViewModels;
using System.Linq;
using System.Collections.Generic;
using FpsGameApi.Models.Database;

namespace FpsGameApi.Test
{
    public abstract class MapperTest
    {
        public MapperTest()
        {
            Mapper = CreateMapper();
        }

        public abstract IMapper CreateMapper();
        public IMapper Mapper { get; private set; }

        public void DbPlayerStats_PlayerStats(DbPlayerStats sample)
        {
            var mapped = Mapper.Map<PlayerStats>(sample);
            
            Assert.AreEqual(sample.AverageMatchesPerDay, mapped.AverageMatchesPerDay);
            Assert.AreEqual(sample.AverageScoreboardPercent, mapped.AverageScoreboardPercent);
            Assert.AreEqual(sample.FavoriteGameMode, mapped.FavoriteGameMode);
            Assert.AreEqual(sample.FavoriteServer, mapped.FavoriteServer);
            Assert.AreEqual(sample.KillToDeathRatio, mapped.KillToDeathRatio);
            Assert.AreEqual(sample.LastMatchPlayed, mapped.LastMatchPlayed);
            Assert.AreEqual(sample.MaximumMatchesPerDay, mapped.MaximumMatchesPerDay);
            Assert.AreEqual(sample.TotalMatchesPlayed, mapped.TotalMatchesPlayed);
            Assert.AreEqual(sample.TotalMatchesWon, mapped.TotalMatchesWon);
            Assert.AreEqual(sample.UniqueServers, mapped.UniqueServers);
        }


        public void PlayerStats_DbPlayerStats(PlayerStats sample)
        {
            var mapped = Mapper.Map<DbPlayerStats>(sample);
            
            Assert.AreEqual(sample.AverageMatchesPerDay, mapped.AverageMatchesPerDay);
            Assert.AreEqual(sample.AverageScoreboardPercent, mapped.AverageScoreboardPercent);
            Assert.AreEqual(sample.FavoriteGameMode, mapped.FavoriteGameMode);
            Assert.AreEqual(sample.FavoriteServer, mapped.FavoriteServer);
            Assert.AreEqual(sample.KillToDeathRatio, mapped.KillToDeathRatio);
            Assert.AreEqual(sample.LastMatchPlayed, mapped.LastMatchPlayed);
            Assert.AreEqual(sample.MaximumMatchesPerDay, mapped.MaximumMatchesPerDay);
            Assert.AreEqual(sample.TotalMatchesPlayed, mapped.TotalMatchesPlayed);
            Assert.AreEqual(sample.TotalMatchesWon, mapped.TotalMatchesWon);
            Assert.AreEqual(sample.UniqueServers, mapped.UniqueServers);
        }

        public void Server_DbServer(Server sample)
        {
            var mapped = Mapper.Map<DbServer>(sample);
            Assert.AreEqual(sample.Endpoint, mapped.Endpoint);
            Assert.AreEqual(sample.Info.Name, mapped.Name);
            Assert2.AreEqualSequences(sample.Info.GameModes,
                mapped.GameModes.Select(x=>x.Contents));
        }


        public void DbServer_Server(DbServer sample)
        {
            var mapped = Mapper.Map<Server>(sample);
            Assert.AreEqual(sample.Endpoint, mapped.Endpoint);
            Assert.AreEqual(sample.Name, mapped.Info.Name);
            Assert2.AreEqualSequences(sample.GameModes.Select(x => x.Contents),
                mapped.Info.GameModes);
        }

        public void DbScoreboardPlayer_ScoreboardPlayer(DbScoreboardPlayer sample)
        {
            var mapped = Mapper.Map<ScoreboardPlayer>(sample);
            Assert.AreEqual(sample.Frags, mapped.Frags);
            Assert.AreEqual(sample.Kills, mapped.Kills);
            Assert.AreEqual(sample.Deaths, mapped.Deaths);
            Assert.AreEqual(sample.Name, mapped.Name);
        }

        public void Match_DbMatch(Match sample)
        {
            var mapped = Mapper.Map<DbMatch>(sample);

            Assert.AreEqual(sample.Server, mapped.Server);
            Assert.AreEqual(sample.Timestamp, mapped.Timestamp);

            Assert.AreEqual(sample.Results.Map, mapped.Map);
            Assert.AreEqual(sample.Results.TimeElapsed, mapped.TimeElapsed);
            Assert.AreEqual(sample.Results.TimeLimit, mapped.TimeLimit);
            Assert.AreEqual(sample.Results.FragLimit, mapped.FragLimit);
            Assert.AreEqual(sample.Results.GameMode, mapped.GameMode);

            Assert2.AreEqualSequences(sample.Results.Scoreboard,
                mapped.Scoreboard.Select(Mapper.Map<ScoreboardPlayer>));
        }

        public void DbMatch_Match(DbMatch sample)
        {
            var mapped = Mapper.Map<Match>(sample);
            
            Assert.AreEqual(sample.Server, mapped.Server);
            Assert.AreEqual(sample.Timestamp, mapped.Timestamp);

            Assert.AreEqual(sample.Map, mapped.Results.Map);
            Assert.AreEqual(sample.TimeElapsed, mapped.Results.TimeElapsed);
            Assert.AreEqual(sample.TimeLimit, mapped.Results.TimeLimit);
            Assert.AreEqual(sample.FragLimit, mapped.Results.FragLimit);
            Assert.AreEqual(sample.GameMode, mapped.Results.GameMode);

            Assert2.AreEqualSequences(sample.Scoreboard
                .Select(Mapper.Map<ScoreboardPlayer>), mapped.Results.Scoreboard);
        }


        public void DbServerStats_ServerStats(DbServerStats sample)
        {
            var mapped = Mapper.Map<ServerStats>(sample);

            Assert.AreEqual(sample.AverageMatchesPerDay, mapped.AverageMatchesPerDay);
            Assert.AreEqual(sample.AveragePopulation, mapped.AveragePopulation);

            Assert.AreEqual(sample.MaximumMatchesPerDay, mapped.MaximumMatchesPerDay);
            Assert.AreEqual(sample.MaximumPopulation, mapped.MaximumPopulation);
            Assert.AreEqual(sample.TotalMatchesPlayed, mapped.TotalMatchesPlayed);

            Assert2.AreEqualSequences(sample.Top5GameModes.Select(x=>x.Contents), mapped.Top5GameModes);
            Assert2.AreEqualSequences(sample.Top5Maps.Select(x => x.Contents), mapped.Top5Maps);
        }


        public void ServerStats_DbServerStats(ServerStats sample)
        {
            var mapped = Mapper.Map<DbServerStats>(sample);

            Assert.AreEqual(sample.AverageMatchesPerDay, mapped.AverageMatchesPerDay);
            Assert.AreEqual(sample.AveragePopulation, mapped.AveragePopulation);

            Assert.AreEqual(sample.MaximumMatchesPerDay, mapped.MaximumMatchesPerDay);
            Assert.AreEqual(sample.MaximumPopulation, mapped.MaximumPopulation);
            Assert.AreEqual(sample.TotalMatchesPlayed, mapped.TotalMatchesPlayed);

            Assert2.AreEqualSequences(sample.Top5GameModes, mapped.Top5GameModes.Select(x => x.Contents));
            Assert2.AreEqualSequences(sample.Top5Maps, mapped.Top5Maps.Select(x => x.Contents));
        }

        [TestMethod]
        public void ServerStats_DbServerStats()
        {
            var serverStats = new ServerStats
            {
                AverageMatchesPerDay = 1.2,
                AveragePopulation = 1.3,
                MaximumMatchesPerDay = 4,
                MaximumPopulation = 5,
                Top5GameModes = new [] { "gm1", "gm2" },
                Top5Maps = new [] { "gm1", "cm1" },
                TotalMatchesPlayed = 6
            };
            ServerStats_DbServerStats(serverStats);
        }


        [TestMethod]
        public void DbServerStats_ServerStats()
        {
            var dbServerStats = new DbServerStats
            {
                AverageMatchesPerDay = 1.2,
                AveragePopulation = 1.3,
                Endpoint = "endpoint",
                MaximumMatchesPerDay = 4,
                MaximumPopulation = 5,
                Top5GameModes = new[] { "gm1", "gm2" }.Select(x => new DbString(x)).ToList(),
                Top5Maps = new[] { "gm1", "cm1" }.Select(x => new DbString(x)).ToList(),
                TotalMatchesPlayed = 6
            };
            DbServerStats_ServerStats(dbServerStats);
        }


        [TestMethod]
        public void DbPlayerStats_PlayerStats()
        {
            var dbPlayerStats = new DbPlayerStats
            {
                AverageMatchesPerDay = 2.73,
                AverageScoreboardPercent = 0.55,
                FavoriteGameMode = "CM",
                FavoriteServer = "server",
                UniqueServers = 2,
                KillToDeathRatio = 2,
                LastMatchPlayed = DateTime.Now,
                MaximumMatchesPerDay = 2,
                Name = "server2",
                TotalMatchesPlayed = 2,
                TotalMatchesWon = 2
            };
            DbPlayerStats_PlayerStats(dbPlayerStats);
        }

        [TestMethod]
        public void PlayerStats_DbPlayerStats()
        {
            var playerStats = new PlayerStats
            {
                AverageMatchesPerDay = 2.73,
                AverageScoreboardPercent = 0.55,
                FavoriteGameMode = "CM",
                FavoriteServer = "server",
                UniqueServers = 2,
                KillToDeathRatio = 2,
                LastMatchPlayed = DateTime.Now,
                MaximumMatchesPerDay = 2,
                TotalMatchesPlayed = 2,
                TotalMatchesWon = 2
            };
            PlayerStats_DbPlayerStats(playerStats);
        }

        [TestMethod]
        public void Match_DbMatch()
        {
            var match = new Match
            {
                Server = "почт.рф:666",
                Timestamp = DateTime.Now,
                Results = new MatchResults {
                    Map = "de_даст",
                    FragLimit = 50,
                    TimeElapsed = 66.789,
                    GameMode = "DE",
                    TimeLimit = 70,
                } 
            };
            match.Results.Scoreboard = new[] {
                new ScoreboardPlayer { Deaths = 1, Kills = 3, Frags = 2, Name = "Phil" },
                new ScoreboardPlayer { Deaths = 3, Kills = 1, Frags = 2, Name = "Phil2" },
            };
            Match_DbMatch(match);
        }

        [TestMethod]
        public void DbScoreboardPlayer_ScoreboardPlayer()
        {
            var dbPlayer = new DbScoreboardPlayer
            {
                Kills = 1,
                Frags = 2,
                Deaths = 3,
                Name = "12ериыфтьа7Алеsssssjah"
            };
            DbScoreboardPlayer_ScoreboardPlayer(dbPlayer);
        }

        [TestMethod]
        public void DbMatch_Match()
        {
            var dbMatch = new DbMatch
            {
                Map = "de_даст",
                FragLimit = 50,
                TimeElapsed = 66.789,
                GameMode = "DE",
                TimeLimit = 70,
                Server = "почт.рф:666",
                Timestamp = DateTime.Now,
            };
            dbMatch.Scoreboard = new[] {
                new DbScoreboardPlayer { 
                    Match_Server = dbMatch.Server, 
                    Match_Timestamp = dbMatch.Timestamp,
                    Deaths = 1,
                    Kills = 1,
                    Frags = 2,
                    Name = @"x'}<><><s><><><//\/:.."
                },
                new DbScoreboardPlayer { 
                    Match_Server = dbMatch.Server, 
                    Match_Timestamp = dbMatch.Timestamp,
                    Deaths = 1,
                    Name = @"b'}<><><><><><//\/:.."
                },
            };
            DbMatch_Match(dbMatch);
        }

        [TestMethod]
        public void Server_ServerDb()
        {
            var server = new Server
            {
                Endpoint = "127.0.0.1:27001",
                Info = new ServerInfo
                {
                    Name = ">My АНЪЮЖУАЛ СЕРВЕРОK;'\"<",
                    GameModes = new[] { "GM1", "GM3", "GM2" }
                }
            };
            Server_DbServer(server);
        }

        [TestMethod]
        public void DbServer_Server()
        {
            var dbServer = new DbServer
            {
                Endpoint = "почта.рф:666",
                Name = "P0STAL><R#VENGE{0}%d%f@;^",
                GameModes = new[] { new DbString("KM"), new DbString("MK") }
            };
            DbServer_Server(dbServer);
        }
    }
}
