﻿using FpsGameApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Test
{
    [TestClass]
    public class AutomapperTest : MapperTest
    {
        public override Models.IMapper CreateMapper()
        {
            return MapperAutoMapper.Instance;
        }
    }
}
