﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Test
{
    public static partial class Assert2
    {
        public static bool AreEqualSequences<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            return a.SequenceEqual(b);
        }

        public static bool AreEqualSets<T>(IEnumerable<T> a, IEnumerable<T> b)
        {
            return new HashSet<T>(a).SetEquals(b);
        }
    }
}