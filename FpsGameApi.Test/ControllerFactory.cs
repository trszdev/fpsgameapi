﻿using FpsGameApi.Controllers;
using FpsGameApi.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FpsGameApi.Test
{
    public class ControllerFactory {

        private readonly StandardKernel kernel;

        public ControllerFactory(bool isDebug)
        {
            kernel = new NinjectKernel(isDebug);
        }

        public ServerController CreateServerController()
        {
            return kernel.Get<ServerController>();
        }

        public MatchController CreateMatchController()
        {
            return kernel.Get<MatchController>();
        }

        public ReportController CreateReportController()
        {
            return kernel.Get<ReportController>();
        }

        public PlayerController CreatePlayerController()
        {
            return kernel.Get<PlayerController>();
        }

    }
}
