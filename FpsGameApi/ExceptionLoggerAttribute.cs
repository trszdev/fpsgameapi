﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace FpsGameApi
{
    public class ExceptionLoggerAttribute : ExceptionFilterAttribute
    {
        protected readonly ILog log;

        public ExceptionLoggerAttribute(bool writeToConsole = false)
        {
            log = LogManager.GetLogger(writeToConsole ? "Console" : "File");
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var exception = actionExecutedContext.Exception;
            log.Fatal(exception);

            base.OnException(actionExecutedContext);
        }
    }
}
