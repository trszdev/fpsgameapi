﻿using FpsGameApi.Models;
using Ninject;
using Ninject.Extensions.Conventions;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi
{
    public class NinjectKernel : StandardKernel
    {
        public NinjectKernel(bool isDebug)
        {
            this.Bind<IMapper>().ToConstant(MapperAutoMapper.Instance);
            this.Bind<IServerStatsService>().To<StatsService>();
            this.Bind<IPlayerStatsService>().To<StatsService>();
            this.Bind<IStatsAggregator>().To<StatsAggregator>();
            this.Bind<PlayerStatsGrabber>().ToSelf();
            this.Bind<ServerStatsGrabber>().ToSelf();

            this.Bind<IReporter>().To<Reporter>();
            if (isDebug)
            {
                var fakeConnection = Effort.DbConnectionFactory.CreateTransient();
                this.Bind<IEntitiesDbContext>().ToConstructor(x => new DbContext(fakeConnection));
                this.Bind<IStatsDbContext>().ToConstructor(x => new DbContext(fakeConnection));
            }
            else
            {
                this.Bind<IStatsDbContext>().ToConstructor(x => new DbContext("FpsServerDbCE"));
                this.Bind<IEntitiesDbContext>().ToConstructor(x => new DbContext("FpsServerDbCE"));
            }

        }
    }
}
