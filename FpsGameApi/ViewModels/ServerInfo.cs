﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FpsGameApi.Models;

namespace FpsGameApi.ViewModels
{
    public class ServerInfo
    {
        public string Name { get; set; }
        public IList<string> GameModes { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as ServerInfo;
            if (that == null) return false;
            return that.Name == this.Name &&
                that.GameModes.SetEqual(this.GameModes);
        }
    }
}
