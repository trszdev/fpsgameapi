﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class Match
    {
        // actually its Server's endpoint
        public string Server { get; set; }
        public DateTime Timestamp { get; set; }
        public MatchResults Results { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as Match;
            if (that == null) return false;
            return that.Server == this.Server &&
                that.Timestamp == this.Timestamp &&
                that.Results.Equals(this.Results);
        }
    }
}
