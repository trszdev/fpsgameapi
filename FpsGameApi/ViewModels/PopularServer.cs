﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class PopularServer
    {
        public string Endpoint { get; set;}
        public string Name { get; set; }
        public double AverageMatchesPerDay { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as PopularServer;
            if (that == null) return false;
            return that.Endpoint == this.Endpoint &&
                that.Name == this.Name &&
                that.AverageMatchesPerDay == this.AverageMatchesPerDay;
        }
    }
}
