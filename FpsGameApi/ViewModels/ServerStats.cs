﻿using FpsGameApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class ServerStats
    {
        public int TotalMatchesPlayed { get; set; }
        public int MaximumMatchesPerDay { get; set; }
        public double AverageMatchesPerDay { get; set; }
        public int MaximumPopulation { get; set; }
        public double AveragePopulation { get; set; }

        // ordered by descending of popularity:
        public IList<string> Top5GameModes { get; set; }
        public IList<string> Top5Maps { get; set; }

        public override string ToString()
        {
            //return "";
            return Stringer.ToString(this);
        }

        public static ServerStats Empty
        {
            get
            {
                return new ServerStats { Top5GameModes = new string[0], Top5Maps = new string[0] };
            }
        }

        public override bool Equals(object obj)
        {
            var that = obj as ServerStats;
            if (that == null) return false;
            return that.TotalMatchesPlayed == this.TotalMatchesPlayed &&
                that.MaximumMatchesPerDay == this.MaximumMatchesPerDay &&
                that.AverageMatchesPerDay == this.AverageMatchesPerDay &&
                that.MaximumPopulation == this.MaximumPopulation &&
                that.AveragePopulation == this.AveragePopulation &&
                that.Top5GameModes.SequenceEqual(this.Top5GameModes) &&
                that.Top5Maps.SequenceEqual(this.Top5Maps);
        }
    }
}
