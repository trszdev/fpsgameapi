﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class BestPlayer
    {
        public string Name { get; set; }
        public double KillToDeathRatio { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as BestPlayer;
            if (that == null) return false;
            return that.Name == this.Name &&
                that.KillToDeathRatio == this.KillToDeathRatio;
        }
    }
}
