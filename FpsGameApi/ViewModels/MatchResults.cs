﻿using FpsGameApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class MatchResults
    {
        public string Map { get; set; }
        public string GameMode { get; set; }
        public int TimeLimit { get; set; }
        public int FragLimit { get; set; }
        public double TimeElapsed { get; set; }
        public IList<ScoreboardPlayer> Scoreboard { get; set; }

        public override string ToString()
        {
            return Stringer.ToString(this);
        }

        public override bool Equals(object obj)
        {
            var that = obj as MatchResults;
            if (that == null) return false;
            return that.Map == this.Map &&
                that.GameMode == this.GameMode &&
                that.TimeLimit == this.TimeLimit &&
                that.FragLimit == this.FragLimit &&
                that.TimeElapsed == this.TimeElapsed &&
                that.Scoreboard.SetEqual(this.Scoreboard);
        }
    }
}
