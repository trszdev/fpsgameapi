﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class Server
    {
        public string Endpoint { get; set; }
        public ServerInfo Info { get; set; }

        public override bool Equals(object obj)
        {
            var that = obj as Server;
            if (that == null) return false;
            return that.Endpoint == this.Endpoint &&
                that.Info.Equals(this.Info);
        }
    }
}
