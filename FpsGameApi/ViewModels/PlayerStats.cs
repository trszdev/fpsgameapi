﻿using FpsGameApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class PlayerStats
    {
        public int TotalMatchesPlayed { get; set; }
        public int TotalMatchesWon { get; set; }
        public string FavoriteServer { get; set; }
        public int UniqueServers { get; set; }
        public string FavoriteGameMode { get; set; }
        public double AverageScoreboardPercent { get; set; }
        public int MaximumMatchesPerDay { get; set; }
        public double AverageMatchesPerDay { get; set; }
        public DateTime LastMatchPlayed { get; set; }
        public double KillToDeathRatio { get; set; }

        public override string ToString()
        {
            return Stringer.ToString(this);
        }

        public static PlayerStats Empty
        {
            get
            {
                return new PlayerStats();
            }
        }

        public override bool Equals(object obj)
        {
            var that = obj as PlayerStats;
            if (that == null) return false;
            return that.TotalMatchesPlayed == this.TotalMatchesPlayed &&
                that.TotalMatchesWon == this.TotalMatchesWon &&
                that.FavoriteServer == this.FavoriteServer &&
                that.UniqueServers == this.UniqueServers &&
                that.FavoriteGameMode == this.FavoriteGameMode &&
                that.AverageScoreboardPercent == this.AverageScoreboardPercent &&
                that.MaximumMatchesPerDay == this.MaximumMatchesPerDay &&
                that.AverageMatchesPerDay == this.AverageMatchesPerDay &&
                that.LastMatchPlayed.Equals(this.LastMatchPlayed) &&
                that.KillToDeathRatio == this.KillToDeathRatio;
        }
    }
}
