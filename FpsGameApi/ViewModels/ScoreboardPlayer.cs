﻿using FpsGameApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.ViewModels
{
    public class ScoreboardPlayer
    {
        public string Name { get; set; }
        public int Frags { get; set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }

        public override string ToString()
        {
            return Stringer.ToString(this);
        }

        public override int GetHashCode()
        {
            return Kills;
        }

        public override bool Equals(object obj)
        {
            var that = obj as ScoreboardPlayer;
            if (that == null) return false;
            return that.Name == this.Name &&
                that.Frags == this.Frags &&
                that.Kills == this.Kills &&
                that.Deaths == this.Deaths;
        }
    }
}
