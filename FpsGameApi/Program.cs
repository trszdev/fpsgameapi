﻿using Microsoft.Owin.Hosting;
using System;

namespace FpsGameApi
{
    class Program
    {
        static string GetPrefix(string[] args)
        {
            if (args.Length >= 2 && "--prefix".Equals(args[0]))
            {
                return args[1];
            }
            else
            {
                throw new ArgumentException("Prefix wasn't specified");
            }
        }

        static void Main(string[] args)
        {
            var prefix = GetPrefix(args);
            if (!prefix.StartsWith("http://"))
                throw new ArgumentException("Prefix must start with \"http://\"");
            using (WebApp.Start<OwinStartup>(url: prefix))
            {
                Console.WriteLine("Running on prefix '{0}'. Press enter to quit", prefix);
                Console.ReadLine();
            } 

        }
    }
}
