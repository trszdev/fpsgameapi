﻿using FpsGameApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FpsGameApi.Models
{
    public class Reporter : IReporter
    {
        protected readonly IEntitiesDbContext Db;
        protected readonly IStatsDbContext StatsDb;

        protected readonly IMapper Mapper;
        public Reporter(IEntitiesDbContext db, IStatsDbContext statsDb, 
            IMapper mapper)
        {
            Mapper = mapper;
            StatsDb = statsDb;
            Db = db;
        }


        public IEnumerable<PopularServer> GetPopularServers(int count)
        {
            var popularServers = StatsDb.ServerStats
                .OrderByDescending(x => x.AveragePopulation)
                .Take(count)
                .ToList();
            return popularServers.Select(x => new PopularServer
            {
                AverageMatchesPerDay = x.AverageMatchesPerDay,
                Endpoint = x.Endpoint,
                Name = Db.Servers.Find(x.Endpoint).Name
            });
        }

        public IEnumerable<BestPlayer> GetBestPlayers(int count)
        {
            var bestPlayers = StatsDb.PlayerStats
                .OrderByDescending(x => x.KillToDeathRatio)
                .Where(x => x.TotalMatchesPlayed >= 10 && x.KillToDeathRatio >= 0)
                .Take(count)
                .ToList();
            return bestPlayers.Select(x => new BestPlayer
            {
                KillToDeathRatio = x.KillToDeathRatio,
                Name = x.Name
            });
        }


        public IEnumerable<Match> GetRecentMatches(int count)
        {
            var recentMatches = Db.Matches
                .OrderByDescending(x => x.Timestamp)
                .Take(count)
                .ToList();
            return recentMatches.Select(Mapper.Map<Match>);
        }
    }
}
