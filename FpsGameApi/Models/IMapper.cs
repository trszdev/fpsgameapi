﻿
namespace FpsGameApi.Models
{
    public interface IMapper
    {
        TDest Map<TDest>(object source);
    }
}
