﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Models
{
    public static class IEnumerableExtensions
    {
        public static bool SetEqual<T>(this IEnumerable<T> a, IEnumerable<T> b)
        {
            return new HashSet<T>(a).SetEquals(b);
        }
    }
}
