﻿using FpsGameApi.Models.Database;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FpsGameApi.Models
{
    public class ServerStatsGrabber
    {
        protected readonly IEntitiesDbContext Db;
        public ServerStatsGrabber(IEntitiesDbContext db)
        {
            Db = db;
        }

        static void FillBasicStats(IQueryable<DbMatch> dbMatches, IDictionary<string, object> stats)
        {
            var dbMatchesPerDay = dbMatches
                .GroupBy(x => DbFunctions.TruncateTime(x.Timestamp))
                .Select(group => group.Count())
                .DefaultIfEmpty(0);
            var dbMatchesPopulation = dbMatches
                .Select(x => x.Scoreboard.Count())
                .DefaultIfEmpty(0);
            var totalMatchesPlayed = dbMatches.Count();
            stats["TotalMatchesPlayed"] = totalMatchesPlayed;
            stats["MaximumMatchesPerDay"] = dbMatchesPerDay.Max();
            stats["MaximumPopulation"] = dbMatchesPopulation.Max();
            stats["AveragePopulation"] = dbMatchesPopulation.Sum() / (double)totalMatchesPlayed;
            stats["AverageMatchesPerDay"] = dbMatchesPerDay.Sum() / (double)totalMatchesPlayed;
        }

        static void GetRidOfNans(IDictionary<string, object> stats)
        {
            foreach (var key in stats.Keys.ToArray())
            {
                var value = stats[key];
                if (value is double && double.IsNaN((double)value))
                    stats[key] = 0D;
            }
        }

        static void FillTop5GameModes(IQueryable<DbMatch> dbMatches, IDictionary<string, object> stats)
        {
            var top5GameModes = dbMatches
                .GroupBy(x => x.GameMode)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key)
                .Take(5);
            stats["Top5GameModes"] = top5GameModes.ToList();
        }

        static void FillTop5Maps(IQueryable<DbMatch> dbMatches, IDictionary<string, object> stats)
        {
            var top5Maps = dbMatches
                .GroupBy(x => x.Map)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key)
                .Take(5);
            stats["Top5Maps"] = top5Maps.ToList();
        }

        public IDictionary<string, object> GrabStats(IQueryable<DbMatch> dbMatches)
        {
            var result = new Dictionary<string, object>();
            FillBasicStats(dbMatches, result);
            FillTop5GameModes(dbMatches, result);
            FillTop5Maps(dbMatches, result);
            GetRidOfNans(result);
            return result;
        }

        public IDictionary<string, object> GrabStats(string endpoint)
        {
            var dbMatches = Db.Matches
                .Include(x => x.Scoreboard)
                .Where(x => x.Server == endpoint);
            return GrabStats(dbMatches);
        }
    }
}
