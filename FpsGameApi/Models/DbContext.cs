﻿using FpsGameApi.Models.Database;
using System.Data.Common;
using System.Data.Entity;
using SqlProviderServices = System.Data.Entity.SqlServer.SqlProviderServices;

namespace FpsGameApi.Models
{
    public class DbContext : System.Data.Entity.DbContext, IEntitiesDbContext, IStatsDbContext
    {
        // Must reference a type in EntityFramework.SqlServer.dll so that this dll will be
        // included in the output folder of referencing projects without requiring a direct 
        // dependency on Entity Framework. See http://stackoverflow.com/a/22315164/1141360.
        private static SqlProviderServices instance = SqlProviderServices.Instance;

        public DbContext(string connectionStringOrName)
            : base(connectionStringOrName)
        { }

        public DbContext(DbConnection conn) : base(conn, false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DbString>()
                .HasOptional<DbServerStats>(x => x.DbServerStats)
                .WithMany(x => x.Top5GameModes);
            modelBuilder.Entity<DbString>()
                .HasOptional<DbServerStats>(x => x.DbServerStats)
                .WithMany(x => x.Top5Maps);
        }

        public IDbSet<DbServer> Servers { get; set; }
        public IDbSet<DbMatch> Matches { get; set; }
        public IDbSet<DbScoreboardPlayer> ScoreboardPlayers { get; set; }
        public IDbSet<DbServerStats> ServerStats { get; set; }
        public IDbSet<DbScoreboardPlayerDetails> ScoreboardPlayerDetails { get; set; }
        public IDbSet<DbPlayerStats> PlayerStats { get; set; }
    }
}
