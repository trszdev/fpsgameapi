﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace FpsGameApi.Models.Database
{
    public class DbServer
    {
        [Key]
        public string Endpoint { get; set; }
        public string Name { get; set; }
        public virtual ICollection<DbString> GameModes { get; set; }
    }
}
