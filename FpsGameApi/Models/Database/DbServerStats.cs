﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Models.Database
{
    public class DbServerStats
    {
        [Key]
        public string Endpoint { get; set; }

        public int TotalMatchesPlayed { get; set; }
        public int MaximumMatchesPerDay { get; set; }
        public double AverageMatchesPerDay { get; set; }
        public int MaximumPopulation { get; set; }
        public double AveragePopulation { get; set; }

        // ordered by descending of popularity:
        public virtual ICollection<DbString> Top5GameModes { get; set; }
        public virtual ICollection<DbString> Top5Maps { get; set; }
    }
}
