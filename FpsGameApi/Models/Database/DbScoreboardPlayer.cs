﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FpsGameApi.Models.Database
{
    public class DbScoreboardPlayer
    {
        [Key, Column(Order = 0), ForeignKey("Match")]
        public string Match_Server { get; set; }

        [Key, Column(Order = 1), ForeignKey("Match")]
        public DateTime Match_Timestamp { get; set; }

        [Key, Column(Order = 2)]
        public string Name { get; set; }

        
        public virtual DbMatch Match { get; set; }

        public int Frags { get; set; }
        public int Kills { get; set; }
        public int Deaths { get; set; }
    }
}
