﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FpsGameApi.Models.Database
{
    public class DbMatch
    {
        [Key, Column(Order = 0)]
        public string Server { get; set; }

        [Key, Column(Order = 1)]
        public DateTime Timestamp { get; set; }

        public string Map { get; set; }
        public string GameMode { get; set; }
        public int TimeLimit { get; set; }
        public int FragLimit { get; set; }
        public double TimeElapsed { get; set; }
        public virtual ICollection<DbScoreboardPlayer> Scoreboard { get; set; }
    }
}
