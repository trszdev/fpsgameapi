﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Models.Database
{
    public class DbScoreboardPlayerDetails
    {
        [Key, Column(Order = 0), ForeignKey("Match")]
        public string Match_Server { get; set; }

        [Key, Column(Order = 1), ForeignKey("Match")]
        public DateTime Match_Timestamp { get; set; }

        [Key, Column(Order = 2)]
        public string Name { get; set; }


        public DbMatch Match { get; set; }

        public bool IsWinner { get; set; }
        public double ScoreboardPercent { get; set; }
    }
}
