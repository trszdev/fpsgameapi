﻿
namespace FpsGameApi.Models.Database
{
    public class DbString
    {
        public DbString() { }
        public DbString(string contents) { Contents = contents; }

        public long Id { get; set; }
        public string Contents { get; set; }

        public virtual DbPlayerStats DbPlayerStats { get; set; }
        public virtual DbServerStats DbServerStats { get; set; }
    }
}
