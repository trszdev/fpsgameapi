﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi.Models
{
    static class Stringer
    {
        public static string ToString(IEnumerable<object> seq)
        {
            return "[" + string.Join(", ", seq.Select(x => x.ToString())) + "]";
        }

        public static string ToString(object obj)
        {
            var type = obj.GetType();
            var result = new List<string>();
            foreach (var propertyInfo in type.GetProperties())
            {
                var name = propertyInfo.Name;
                var value = propertyInfo.GetValue(obj);
                
                if (value is IEnumerable<object>)
                    result.Add(name + " = " + ToString(value as IEnumerable<object>));
                else if (value is int || value is double || value is string || value is DateTime)
                    result.Add(name + " = " + value);
                
            }
            return string.Format("{0} {{ {1} }}", type.Name, string.Join("; \n", result));
        }

    }
}
