﻿using FpsGameApi.Models.Database;
using System.Data.Entity;

namespace FpsGameApi.Models
{
    public interface IStatsDbContext : IDbContext
    {
        IDbSet<DbServerStats> ServerStats { get; }
        IDbSet<DbScoreboardPlayerDetails> ScoreboardPlayerDetails { get; }
        IDbSet<DbPlayerStats> PlayerStats { get; }
    }
}
