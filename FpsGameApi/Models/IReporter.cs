﻿using FpsGameApi.ViewModels;
using System.Collections.Generic;

namespace FpsGameApi.Models
{
    public interface IReporter
    {
        IEnumerable<PopularServer> GetPopularServers(int count);
        IEnumerable<BestPlayer> GetBestPlayers(int count);
        IEnumerable<Match> GetRecentMatches(int count);
    }
}
