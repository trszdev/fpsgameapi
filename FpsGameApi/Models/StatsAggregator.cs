﻿using FpsGameApi.Models.Database;
using FpsGameApi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Migrations;

namespace FpsGameApi.Models
{
    public class StatsAggregator : IStatsAggregator
    {
        protected readonly IMapper Mapper;
        protected readonly IStatsDbContext Db;
        protected readonly ServerStatsGrabber ServerStatsGrabber;
        protected readonly PlayerStatsGrabber PlayerStatsGrabber;


        public StatsAggregator(IStatsDbContext db, IMapper mapper,
            ServerStatsGrabber sg, PlayerStatsGrabber pg)
        {
            Mapper = mapper;
            ServerStatsGrabber = sg;
            PlayerStatsGrabber = pg;
            Db = db;
        }



        void AddPlayers(Match match)
        {
            var count = match.Results.Scoreboard.Count - 1D;
            
            if (count == 0)
            {
                var player = match.Results.Scoreboard[0];
                Db.ScoreboardPlayerDetails.Add(new DbScoreboardPlayerDetails
                {
                    Match_Server = match.Server,
                    Match_Timestamp = match.Timestamp,
                    Name = player.Name,
                    ScoreboardPercent = 100D,
                    IsWinner = true,
                });
            }
            else
            {
                var i = 0;
                foreach (var player in match.Results.Scoreboard
                    .OrderBy(x => x.Kills)
                    .ThenBy(x => x.Frags)
                    .ThenByDescending(x => x.Deaths))
                {
                    Db.ScoreboardPlayerDetails.Add(new DbScoreboardPlayerDetails
                    {
                        Match_Server = match.Server,
                        Match_Timestamp = match.Timestamp,
                        Name = player.Name,
                        ScoreboardPercent = (i / count) * 100D,
                        IsWinner = i == count,
                    });
                    i++;
                }
            }
            Db.SaveChanges();
        }


        void RefreshServerStats(Match match)
        {
            var dictStats = ServerStatsGrabber.GrabStats(match.Server);
            var serverStats = new ServerStats
            {
                AverageMatchesPerDay = (double)dictStats["AverageMatchesPerDay"],
                AveragePopulation = (double)dictStats["AveragePopulation"],
                MaximumMatchesPerDay = (int)dictStats["MaximumMatchesPerDay"],
                MaximumPopulation = (int)dictStats["MaximumPopulation"],
                Top5GameModes = (IList<string>)dictStats["Top5GameModes"],
                Top5Maps = (IList<string>)dictStats["Top5Maps"],
                TotalMatchesPlayed = (int)dictStats["TotalMatchesPlayed"],
            };
            var dbServerStats = Mapper.Map<DbServerStats>(serverStats);
            dbServerStats.Endpoint = match.Server;  
            

            // Surprisingly i can't just call AddOrUpdate, because DbServerStats
            // contains data from other tables (Icollection<Dbstring>).
            // expected that EF code first should do it, but it doesnt
            // so I DONT TAKE CARE FOR HACK BELOW 
            
            var found = Db.ServerStats.Find(match.Server);
            
            if (found == null)
            {
                Db.ServerStats.Add(dbServerStats);
            }
            else
            {
                Db.ServerStats.AddOrUpdate(dbServerStats);
                found.Top5GameModes.Clear();
                found.Top5Maps.Clear();
                found.Top5GameModes = dbServerStats.Top5GameModes;
                found.Top5Maps = dbServerStats.Top5Maps;
            }
            
        }


        void RefreshPlayerStats(Match match, string name)
        {
            var dictStats = PlayerStatsGrabber.GrabStats(name);
            var total = (int)dictStats["TotalMatchesPlayed"];
            var playerStats = new PlayerStats
            {
                AverageMatchesPerDay = (double)dictStats["AverageMatchesPerDay"],
                AverageScoreboardPercent = Db.ScoreboardPlayerDetails
                    .Where(x=>x.Name==name)
                    .Select(x=>x.ScoreboardPercent)
                    .DefaultIfEmpty(0).Sum() / total,
                FavoriteServer = (string)dictStats["FavoriteServer"],
                FavoriteGameMode = (string)dictStats["FavoriteGameMode"],
                KillToDeathRatio = (double)dictStats["KillToDeathRatio"],
                TotalMatchesWon = Db.ScoreboardPlayerDetails.Count(x => x.IsWinner),
                UniqueServers = (int)dictStats["UniqueServers"],
                LastMatchPlayed = (DateTime)dictStats["LastMatchPlayed"],
                MaximumMatchesPerDay = (int)dictStats["MaximumMatchesPerDay"],
                TotalMatchesPlayed = total
            };
            var dbPlayerStats = Mapper.Map<DbPlayerStats>(playerStats);
            dbPlayerStats.Name = name;
            Db.PlayerStats.AddOrUpdate(dbPlayerStats);
        }


        void RefreshPlayerStats(Match match)
        {
            foreach (var name in match.Results.Scoreboard.Select(x => x.Name))
            {
                RefreshPlayerStats(match, name);
            }
        }


        public void AddMatch(Match match)
        {
            AddPlayers(match);
            RefreshPlayerStats(match);
            RefreshServerStats(match);
            Db.SaveChanges();
        }
    }
}
