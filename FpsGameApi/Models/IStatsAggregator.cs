﻿using FpsGameApi.ViewModels;

namespace FpsGameApi.Models
{
    public interface IStatsAggregator
    {
        void AddMatch(Match match);
    }
}
