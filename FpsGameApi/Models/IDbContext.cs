﻿using System;
using System.Threading.Tasks;

namespace FpsGameApi.Models
{
    public interface IDbContext : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
