﻿using FpsGameApi.ViewModels;
using System.Linq;
using System.Data.Entity;


namespace FpsGameApi.Models
{
    public class StatsService : IServerStatsService, IPlayerStatsService
    {
        protected readonly IStatsDbContext Db;
        protected readonly IMapper Mapper;

        public StatsService(IStatsDbContext db, IMapper mapper)
        {
            Mapper = mapper;
            Db = db;
        }

        ServerStats IServerStatsService.GetStats(string serverEndpoint)
        {
            var dbServerStats = Db.ServerStats
                .Include(x => x.Top5GameModes)
                .Include(x => x.Top5Maps)
                .SingleOrDefault(x => x.Endpoint == serverEndpoint);
            if (dbServerStats == null)
                return ServerStats.Empty;
            return Mapper.Map<ServerStats>(dbServerStats);
        }

        PlayerStats IPlayerStatsService.GetStats(string playerName)
        {
            var dbPlayerStats = Db.PlayerStats
                .SingleOrDefault(x => x.Name == playerName);
            if (dbPlayerStats == null)
                return PlayerStats.Empty;
            return Mapper.Map<PlayerStats>(dbPlayerStats);
        }
    }
}
