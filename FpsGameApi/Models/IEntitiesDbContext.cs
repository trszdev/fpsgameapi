﻿using FpsGameApi.Models.Database;
using System.Data.Entity;

namespace FpsGameApi.Models
{
    public interface IEntitiesDbContext: IDbContext
    {
        IDbSet<DbServer> Servers { get; }
        IDbSet<DbMatch> Matches { get; }
        IDbSet<DbScoreboardPlayer> ScoreboardPlayers { get; }
    }
}
