﻿using FpsGameApi.ViewModels;
using System.Linq;
using AutoMapper;
using FpsGameApi.Models.Database;

namespace FpsGameApi.Models
{
    public class MapperAutoMapper : IMapper
    {
        public static readonly MapperAutoMapper Instance = new MapperAutoMapper();


        private MapperAutoMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DbServer, ServerInfo>()
                    .ForMember(f => f.GameModes, o => o.MapFrom(
                        x => x.GameModes.Select(v => v.Contents)
                    ));

                cfg.CreateMap<DbServer, Server>()
                    .ForMember(f => f.Info, o => o.MapFrom(x => x));

                cfg.CreateMap<Server, DbServer>()
                    .ForMember(f => f.GameModes, o => o.MapFrom(x => x.Info.GameModes.Select(v => new DbString(v)).ToList()))
                    .ForMember(f => f.Name, o => o.MapFrom(x => x.Info.Name));

                
                cfg.CreateMap<ScoreboardPlayer, DbScoreboardPlayer>();
                cfg.CreateMap<DbScoreboardPlayer, ScoreboardPlayer>();

                cfg.CreateMap<DbMatch, MatchResults>();
                cfg.CreateMap<MatchResults, DbMatch>();

                cfg.CreateMap<DbPlayerStats, PlayerStats>();
                cfg.CreateMap<PlayerStats, DbPlayerStats>();

                cfg.CreateMap<DbServerStats, ServerStats>()
                    .ForMember(f => f.Top5GameModes, o => o.MapFrom(
                        x => x.Top5GameModes.Select(v => v.Contents)
                     ))
                    .ForMember(f => f.Top5Maps, o => o.MapFrom(
                        x => x.Top5Maps.Select(v => v.Contents)
                     ));

                cfg.CreateMap<ServerStats, DbServerStats>()
                    .ForMember(f => f.Top5GameModes, o => o.MapFrom(x => x.Top5GameModes.Select(v => new DbString(v)).ToList()))
                    .ForMember(f => f.Top5Maps, o => o.MapFrom(x => x.Top5Maps.Select(v => new DbString(v)).ToList()));

                cfg.CreateMap<DbMatch, Match>()
                    .ForMember(f => f.Results, o => o.MapFrom(x => x));

                cfg.CreateMap<Match, DbMatch>()
                    .ForMember(f => f.Map, o => o.MapFrom(x => x.Results.Map))
                    .ForMember(f => f.TimeLimit, o => o.MapFrom(x => x.Results.TimeLimit))
                    .ForMember(f => f.Scoreboard, o => o.MapFrom(x => x.Results.Scoreboard))
                    .ForMember(f => f.GameMode, o => o.MapFrom(x => x.Results.GameMode))
                    .ForMember(f => f.TimeElapsed, o => o.MapFrom(x => x.Results.TimeElapsed))
                    .ForMember(f => f.FragLimit, o => o.MapFrom(x => x.Results.FragLimit));
            });
        }

        public TDest Map<TDest>(object source)
        {
            return Mapper.Map<TDest>(source);
        }
    }
}
