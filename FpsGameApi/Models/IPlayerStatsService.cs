﻿using FpsGameApi.ViewModels;

namespace FpsGameApi.Models
{
    public interface IPlayerStatsService
    {
        PlayerStats GetStats(string playerName);
    }
}
