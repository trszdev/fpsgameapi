﻿using FpsGameApi.ViewModels;

namespace FpsGameApi.Models
{
    public interface IServerStatsService
    {
        ServerStats GetStats(string serverEndpoint);
    }
}
