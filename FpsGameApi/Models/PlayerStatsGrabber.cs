﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace FpsGameApi.Models
{
    
    public class PlayerStatsGrabber
    {

        protected IEntitiesDbContext Db;

        public PlayerStatsGrabber(IEntitiesDbContext db)
        {
            Db = db;
        }

        public IDictionary<string, object> GrabStats(string playerName)
        {
            var dbMatches = Db.ScoreboardPlayers
                .Include(x => x.Match)
                .Where(x => x.Name == playerName)
                .Select(x => x.Match);

            var uniqueServers = dbMatches
                .GroupBy(x => x.Server)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key);

            var favoriteGameMode = dbMatches
                .GroupBy(x => x.GameMode)
                .OrderByDescending(x => x.Count())
                .Select(x => x.Key)
                .DefaultIfEmpty("")
                .First();
            
            var dbMatchesPerDay = dbMatches
                .GroupBy(x => DbFunctions.TruncateTime(x.Timestamp))
                .Select(group => group.Count())
                .DefaultIfEmpty(0);

            var dbScoreboard = Db.ScoreboardPlayers.Where(x=>x.Name==playerName);



            var result = new Dictionary<string, object>();
            var totalMatchesPlayed = dbMatches.Count();
            var lastMatchPlayed = dbMatches.OrderByDescending(x => x.Timestamp).FirstOrDefault();
            result["TotalMatchesPlayed"] = totalMatchesPlayed;
            result["UniqueServers"] = uniqueServers.Count();
            result["FavoriteServer"] = uniqueServers.DefaultIfEmpty("").First();
            result["FavoriteGameMode"] = favoriteGameMode;
            result["MaximumMatchesPerDay"] = dbMatchesPerDay.Max();
            result["AverageMatchesPerDay"] = dbMatchesPerDay.Sum() / (double)totalMatchesPlayed;
            if (lastMatchPlayed == null)
                result["LastMatchPlayed"] = DateTime.MaxValue;
            else
                result["LastMatchPlayed"] = lastMatchPlayed.Timestamp;
            var kills = dbScoreboard.Select(x => x.Kills).DefaultIfEmpty(0).Sum();
            var deaths = dbScoreboard.Select(x => x.Deaths).DefaultIfEmpty(0).Sum();


            result["KillToDeathRatio"] = deaths == 0 ? -kills-1 : (double)kills / deaths;
            return result;
        }
    }
}
