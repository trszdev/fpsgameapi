﻿using FpsGameApi.ViewModels;
using System.Web.Http;

namespace FpsGameApi.Controllers
{
    public static class ControllerExtensions
    {
        public static IHttpActionResult AddMatch(this MatchController c, Match match)
        {
            return c.AddMatch(match.Server, match.Timestamp, match.Results);
        }

        public static IHttpActionResult AddServer(this ServerController c, Server server)
        {
            return c.AddServer(server.Endpoint, server.Info);
        }
    }
}
