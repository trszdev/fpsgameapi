﻿using FpsGameApi.Models;
using FpsGameApi.ViewModels;
using System.Collections.Generic;
using System.Web.Http;

namespace FpsGameApi.Controllers
{
    public class ReportController : ApiController
    {
        protected readonly IReporter Reporter;
        public ReportController(IReporter reporter)
        {
            Reporter = reporter;
        }

        static int UpdateCount(int count)
        {
            if (count <= 0) return 0;
            else if (count >= 50) return 50;
            return count;
        }

        [Route("reports/recent-matches/{count?}"), HttpGet]
        public IEnumerable<Match> RecentMatches(int count = 5)
        {
            count = UpdateCount(count);
            return Reporter.GetRecentMatches(count);
        }

        [Route("reports/best-players/{count?}"), HttpGet]
        public IEnumerable<BestPlayer> BestPlayers(int count = 5)
        {
            count = UpdateCount(count);
            return Reporter.GetBestPlayers(count);
        }

        [Route("reports/popular-servers/{count?}"), HttpGet]
        public IEnumerable<PopularServer> PopularServers(int count = 5)
        {
            count = UpdateCount(count);
            return Reporter.GetPopularServers(count);
        }
    }
}
