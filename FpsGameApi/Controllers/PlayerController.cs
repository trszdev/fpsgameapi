﻿using FpsGameApi.Models;
using System.Web.Http;
using FpsGameApi.ViewModels;

namespace FpsGameApi.Controllers
{
    public class PlayerController : ApiController
    {
        protected readonly IPlayerStatsService StatsService;

        public PlayerController(IPlayerStatsService statsService)
        {
            StatsService = statsService;
        }

        [Route("players/{name}/stats"), HttpGet]
        public PlayerStats Stats(string name)
        {
            return StatsService.GetStats(name);
        }
    }
}
