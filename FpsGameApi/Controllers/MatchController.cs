﻿using FpsGameApi.Models;
using FpsGameApi.ViewModels;
using System;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Net;
using FpsGameApi.Models.Database;

namespace FpsGameApi.Controllers
{
    [RoutePrefix("servers/{endpoint}")]
    public class MatchController : ApiController
    {
        protected readonly IMapper Mapper;
        protected readonly IEntitiesDbContext Db;
        protected readonly IStatsAggregator StatsAggregator;

        public MatchController(IMapper mapper, IEntitiesDbContext db, IStatsAggregator statsAggregator)
        {
            Mapper = mapper;
            Db = db;
            StatsAggregator = statsAggregator;
        }


        [Route("matches/{timestamp}"), HttpPut]
        public IHttpActionResult AddMatch(string endpoint, DateTime timestamp, [FromBody]MatchResults matchResults)
        {
            var dbServer = Db.Servers.Find(endpoint);
            if (dbServer == null) return NotFound();
            if (Db.Matches.Find(endpoint, timestamp) != null)
                return StatusCode(HttpStatusCode.Conflict);
            var match = new Match { 
                Results = matchResults,
                Server = dbServer.Endpoint,
                Timestamp = timestamp
            };
            var dbMatch = Mapper.Map<DbMatch>(match);
            Db.Matches.Add(dbMatch);
            Db.SaveChanges();
            StatsAggregator.AddMatch(match);
            return Ok();
        }


        [Route("matches/{timestamp}"), HttpGet]
        public object GetMatch(string endpoint, DateTime timestamp)
        {
            var dbMatch = Db.Matches
                .Include(x => x.Scoreboard)
                .SingleOrDefault(x => x.Server == endpoint && x.Timestamp == timestamp);
            if (dbMatch == null) return NotFound();
            return Mapper.Map<MatchResults>(dbMatch);
        }
    }
}
