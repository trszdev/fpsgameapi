﻿using FpsGameApi.Models;
using FpsGameApi.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Net;
using FpsGameApi.Models.Database;

namespace FpsGameApi.Controllers
{
    public class ServerController : ApiController
    {
        protected readonly IMapper Mapper;
        protected readonly IEntitiesDbContext Db;
        protected readonly IServerStatsService StatsService;

        public ServerController(IMapper mapper, IEntitiesDbContext db,
            IServerStatsService statsService)
        {
            Mapper = mapper;
            Db = db;
            StatsService = statsService;
        }


        [Route("servers/{endpoint}/info"), HttpPut]
        public IHttpActionResult AddServer(string endpoint, [FromBody]ServerInfo serverInfo)
        {
            var server = new Server { Endpoint = endpoint, Info = serverInfo };
            if (Db.Servers.Find(endpoint) != null)
                return StatusCode(HttpStatusCode.Conflict);
            var dbServer = Mapper.Map<DbServer>(server);
            Db.Servers.Add(dbServer);
            Db.SaveChanges();
            return Ok();
        }

        [Route("servers/{endpoint}/info"), HttpGet]
        public object GetServerInfo(string endpoint)
        {
            var dbServer = Db.Servers
                    .Include(x => x.GameModes)
                    .FirstOrDefault(x => x.Endpoint == endpoint);
            if (dbServer == null) return NotFound();
            var serverInfo = Mapper.Map<ServerInfo>(dbServer);
            return serverInfo;
        }

        [Route("servers/{endpoint}/stats"), HttpGet]
        public ServerStats Stats(string endpoint)
        {
            return StatsService.GetStats(endpoint);
        }

        [Route("servers/info"), HttpGet]
        public IEnumerable<Server> GetAllServerInfos()
        {
            var servers = Db.Servers
                .Include(x => x.GameModes)
                .ToList()
                .Select(Mapper.Map<Server>);
            return servers;
        } 
    }
}
