﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FpsGameApi
{
    public class ExceptionLogger : System.Web.Http.ExceptionHandling.ExceptionLogger
    {
        protected readonly ILog log;

        public ExceptionLogger(bool writeToConsole = false)
        {
            log = LogManager.GetLogger(writeToConsole ? "Console" : "File");
        }

        public override void Log(System.Web.Http.ExceptionHandling.ExceptionLoggerContext context)
        {
            log.Error(context.ExceptionContext.Exception.ToString());
            //base.Log(context);
        }
    }
}
