﻿using FpsGameApi.Models;
using log4net;
using log4net.Config;
using Newtonsoft.Json.Serialization;
using Owin;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using WebApiContrib.IoC.Ninject;

namespace FpsGameApi
{

    public class OwinStartup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        // https://docs.microsoft.com/en-us/aspnet/web-api/overview/hosting-aspnet-web-api/use-owin-to-self-host-web-api
        public void Configuration(IAppBuilder appBuilder)
        {
            XmlConfigurator.Configure();
            var config = new HttpConfiguration();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

#if DEBUG
            config.Services.Add(typeof(System.Web.Http.ExceptionHandling.IExceptionLogger), new ExceptionLogger(true));
            config.DependencyResolver = new NinjectResolver(new NinjectKernel(isDebug: true));
#else
            config.Services.Add(typeof(System.Web.Http.ExceptionHandling.IExceptionLogger), new ExceptionLogger(false));
            config.DependencyResolver = new NinjectResolver(new NinjectKernel(isDebug: false));
#endif

            config.MapHttpAttributeRoutes();


            config.Filters.Add(new ValidateModelAttribute());

            config.Formatters.Clear();
            var formatter = new JsonMediaTypeFormatter();
            formatter.SerializerSettings.ContractResolver = 
                new CamelCasePropertyNamesContractResolver();
            formatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));
            config.Formatters.Add(formatter);
            
            
            appBuilder.UseWebApi(config);
            config.EnsureInitialized();
        }
    }
}
